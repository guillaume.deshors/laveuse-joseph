#include "SpeedyStepper_modif.h"

// https://wokwi.com/projects/345336325986058834

#define PIN_M1_STEP 2
#define PIN_M1_DIRECTION 3

#define PIN_M1_LIMIT_LOW 10
#define PIN_M1_LIMIT_HIGH 11

#define PIN_M2_STEP 4
#define PIN_M2_DIRECTION 5

#define PIN_M2_LIMIT_LOW 12
#define PIN_M2_LIMIT_HIGH 13

#define STEPS_PER_MM 5.54

// driver is configured with 400 steps/t
#define MOTOR_SPEED_HOMING 100
#define MOTOR_MAX_SPEED 1000 // mm/s
#define MOTOR_ACCEL 4000     // mm/s2

#define FULL_COURSE 801      // mm

#define PIN_ANALOG_IN A0

const int LED_PIN = 13;

SpeedyStepper m1;
SpeedyStepper m2;

void setup()
{
  Serial.begin(115200);
  Serial.println("Begin");

  pinMode(PIN_M1_LIMIT_LOW, INPUT);
  pinMode(PIN_M1_LIMIT_HIGH, INPUT);
  pinMode(PIN_M2_LIMIT_LOW, INPUT);
  pinMode(PIN_M2_LIMIT_HIGH, INPUT);

  pinMode(LED_PIN, OUTPUT);

  m1.connectToPins(PIN_M1_STEP, PIN_M1_DIRECTION);
  m2.connectToPins(PIN_M2_STEP, PIN_M2_DIRECTION);

  //
  // First you must tell the library "how many steps it takes to move one
  // millimeter".  My setup is configured with a lead-screw having
  // 25 full-steps/mm and 1x microstepping.
  //
  m1.setStepsPerMillimeter(STEPS_PER_MM);
  m2.setStepsPerMillimeter(STEPS_PER_MM);

  m1.setSpeedInMillimetersPerSecond(MOTOR_MAX_SPEED);
  m1.setAccelerationInMillimetersPerSecondPerSecond(MOTOR_ACCEL);

  m2.setSpeedInMillimetersPerSecond(MOTOR_MAX_SPEED);
  m2.setAccelerationInMillimetersPerSecondPerSecond(MOTOR_ACCEL);

  Serial.println("Home m1...");
  if (!m1.moveToHomeInMillimeters(-1, MOTOR_SPEED_HOMING, FULL_COURSE * 1.1, PIN_M1_LIMIT_LOW))
  {
    error();
  }
  
  Serial.println("Home m2...");
  if (!m2.moveToHomeInMillimeters(-1, MOTOR_SPEED_HOMING, FULL_COURSE * 1.1, PIN_M2_LIMIT_LOW))
  {
    error();
  }
  

  Serial.println("Home OK. Starting loop...");
}

void error()
{
  //
  // this code is executed only if homing fails because it has moved farther
  // than maxHomingDistanceInMM and never finds the limit switch, blink the
  // LED fast forever indicating a problem
  //
  while (true)
  {
    digitalWrite(LED_PIN, HIGH);
    delay(50);
    digitalWrite(LED_PIN, LOW);
    delay(50);
  }
}

/**
 * in the main loop, make the motors go from home to home - FULL_COURSE
 * and each time it's over do the opposite.
 */
void loop()
{

  changeSpeed();

  m1.setupRelativeMoveInMillimeters(FULL_COURSE);
  m2.setupRelativeMoveInMillimeters(FULL_COURSE);

  while (!(m1.motionComplete() && m2.motionComplete()))
  {
    m1.processMovement();
    m2.processMovement();
  }

  changeSpeed();

  m1.setupRelativeMoveInMillimeters(-FULL_COURSE);
  m2.setupRelativeMoveInMillimeters(-FULL_COURSE);

  while (!(m1.motionComplete() && m2.motionComplete()))
  {
    m1.processMovement();
    m2.processMovement();
  }

  changeSpeed();
}

void changeSpeed()
{
  // read potentiometer and adjust speed
  int analog_in = 0;
  // if zero : just wait
  while (analog_in == 0)
  {
    analog_in = analogRead(PIN_ANALOG_IN);
    Serial.print("speed = ");
    Serial.println(map(analog_in, 0, 1023, MOTOR_MAX_SPEED / 3, MOTOR_MAX_SPEED));
  }
  // do not put a speed of zero or else it won't move ever again
  m1.setSpeedInMillimetersPerSecond(map(analog_in, 0, 1023, MOTOR_MAX_SPEED / 3, MOTOR_MAX_SPEED));
  m2.setSpeedInMillimetersPerSecond(map(analog_in, 0, 1023, MOTOR_MAX_SPEED / 3, MOTOR_MAX_SPEED));
}
